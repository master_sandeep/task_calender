import React from "react";
import dateFns from "date-fns";

class Calendar extends React.Component {
  state = {
    currentYear: new Date().getFullYear(),
    currentDate: new Date().getDate(),
    currentMonth: new Date().getMonth(),
    scheduledDate: new Date(2020, 5, 20)
  };

  daysmap = ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT']
  renderHeader() {
    return (
      <div className="header row flex-middle" style={{ height: '100%' }}>
        <div className="col col-start">
          <div className="icon" onClick={this.prevYear}>
            chevron_left
          </div>
        </div>
        <div className="col col-center">
          <span>{this.state.currentYear}</span>
        </div>
        <div className="col col-end" onClick={this.nextYear}>
          <div className="icon">chevron_right</div>
        </div>
      </div>
    );
  }

  renderDate() {
    let dates = [];
    let rows = [];

    for (let i = 1; i < 8; i++) {
      dates = [];
      let count = i;
      for (let j = 0; j < 5; j++) {
        let date = i + 7 * j;
        date = date > 31 ? '' : date;
        let classNameString = "col col-center day";
        if (date === 30) {
          classNameString = "col col-center purple"
        } if (date === 31) {
          classNameString = "col col-center day-31"
        }
        let isLeafyear = dateFns.isLeapYear(new Date(this.state.currentYear, 1, 1))
        if (isLeafyear && date === 29) {
          classNameString = "col col-center yellow"
        } else if (!isLeafyear && date === 28) {
          classNameString = "col col-center yellow"
        }

        if (this.state.currentDate === date && this.state.currentYear === new Date().getFullYear()) {
          classNameString += ' current-date'
        }
        let isSeduleDate = this.state.scheduledDate.getDate() === date && this.state.scheduledDate.getFullYear() === this.state.currentYear;
        if (isSeduleDate) {
          classNameString += ' dropdown dropbtn'
        }
        dates.push(
          <div className={classNameString} key={i + count}>
            {date}
            {isSeduleDate && <span>
              <span>.</span>
              <div class="dropdown-content">
                <div className='row'>
                  <div className="col col-start">
                    - To Do List
                </div>
                  <div className="col col-end" onClick={this.nextYear}>
                    <div className="icon red">close</div>
                  </div>
                </div>
                <div className='row'>
                  <div className="col col-start">
                    - Meeting
                </div>
                  <div className="col col-end" onClick={this.nextYear}>
                    <div className="icon red">close</div>
                  </div>
                </div>
                <button class="dropbtn" style={{ marginTop: '10px' }}>+Add</button>
              </div>
            </span>}
          </div>
        );
        count++;
      }
      rows.push(<div className="days row" key={'daterow' + i}>{dates}</div>);
    }

    return rows;
  }

  renderDays() {
    let days = [];
    let rows = [];

    for (let i = 1; i < 8; i++) {
      days = [];
      let count = i;
      for (let j = 0; j < 7; j++) {
        if (count === 7) {
          count = 0;
        }
        let classNameString = count !== 0 ? "col col-center day" : "col col-center sun-day"
        days.push(
          <div className={classNameString} key={i + count}>
            {this.daysmap[count]}
          </div>
        );
        count++;
      }
      rows.push(<div className="days row" key={'dayrow' + i}>{days}</div>);
    }

    return rows;
  }

  renderMonth() {
    const { currentYear } = this.state;
    let monthStatus = [];
    for (let i = 0; i < 12; i++) {
      const startDate = new Date(currentYear, i).getDay();
      monthStatus.push(startDate);
    }

    let rows = [];
    let months = [];
    for (let i = 1; i < 4; i++) {
      months = [];
      let count = 1;
      for (let j = 0; j < 7; j++) {
        if (count === 7) {
          count = 0;
        }
        let classNameString = "col col-center yellow"
        let numberOfDays;
        let monthIndex = monthStatus.indexOf(count);
        let monthString = '';
        if (monthIndex !== -1) {
          let tempMonth = new Date(currentYear, monthIndex);
          numberOfDays = dateFns.getDaysInMonth(tempMonth);
          if (numberOfDays === 30) {
            classNameString = "col col-center purple"
          } if (numberOfDays === 31) {
            classNameString = "col col-center day-31"
          }
          monthString = dateFns.format(tempMonth, 'MMM')
          monthStatus.splice(monthIndex, 1, '');
        }
        months.push(
          <div className={classNameString} key={'month' + i + j} onClick={() => { numberOfDays && alert(numberOfDays) }}>
            {monthString}
          </div>
        );
        count++;
      }
      rows.push(<div className="days row" key={'month' + i}>{months}</div>);
    }
    return rows;
  }


  nextYear = () => {
    this.setState({
      currentYear: this.state.currentYear + 1
    });
  };

  prevYear = () => {
    this.setState({
      currentYear: this.state.currentYear - 1
    });
  };

  render() {
    return (
      <div className="calendar">

        <div className="row">
          <div className='col-3 col-center'>{this.renderHeader()}</div>
          <div className='col-9 col-center'>{this.renderMonth()}</div>
        </div>
        <div className="row">
          <div className='col-3 col-center'>{this.renderDate()}</div>
          <div className='col-9 col-center'>{this.renderDays()}</div>
        </div>
      </div>
    );
  }
}

export default Calendar;
